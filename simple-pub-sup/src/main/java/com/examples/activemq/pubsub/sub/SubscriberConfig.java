package com.examples.activemq.pubsub.sub;


import javax.jms.ConnectionFactory;
import javax.validation.constraints.NotNull;

import com.examples.activemq.pubsub.configs.ItemSubscriberConfiguration;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.jms.DefaultJmsListenerContainerFactoryConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.config.DefaultJmsListenerContainerFactory;
import org.springframework.jms.config.JmsListenerContainerFactory;
import org.springframework.jms.support.converter.MessageConverter;

@EnableJms
@Configuration
@Import(ItemSubscriberConfiguration.class)
class SubscriberConfig {

    @Bean
    @SuppressWarnings("SpringJavaAutowiringInspection")
    public JmsListenerContainerFactory<?> listenerContainerFactory(MessageConverter messageConverter,
                                                                   ConnectionFactory connectionFactory,
                                                                   DefaultJmsListenerContainerFactoryConfigurer configurer,
                                                                   @NotNull @Value("${destination.type}") String destinationType) {
        DefaultJmsListenerContainerFactory factory = new DefaultJmsListenerContainerFactory();
        factory.setMessageConverter(messageConverter);
        configurer.configure(factory, connectionFactory);
        if (destinationType.equals("topic")) {
            factory.setPubSubDomain(true);
        } else {
            factory.setPubSubDomain(false);
        }
        return factory;
    }

}