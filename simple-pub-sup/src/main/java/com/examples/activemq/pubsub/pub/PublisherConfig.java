package com.examples.activemq.pubsub.pub;

import javax.jms.ConnectionFactory;
import javax.validation.constraints.NotNull;

import com.examples.activemq.pubsub.configs.ItemPublisherConfiguration;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.support.converter.MessageConverter;

@EnableJms
@Configuration
@Import(ItemPublisherConfiguration.class)
class PublisherConfig {

    @Bean
    public JmsTemplate jmsTemplate(MessageConverter messageConverter,
                                   ConnectionFactory connectionFactory,
                                   @NotNull @Value("${destination.type}") String destinationType) {
        JmsTemplate template = new JmsTemplate();
        template.setConnectionFactory(connectionFactory);
        template.setMessageConverter(messageConverter);
        if (destinationType.equals("topic")) {
            template.setPubSubDomain(true);
        } else {
            template.setPubSubDomain(false);
        }
        return template;
    }

}