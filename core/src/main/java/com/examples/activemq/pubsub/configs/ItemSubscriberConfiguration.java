package com.examples.activemq.pubsub.configs;

import javax.validation.constraints.NotNull;
import java.util.concurrent.Executor;

import com.examples.activemq.pubsub.services.ItemJmsSubscriber;
import com.examples.activemq.pubsub.services.ItemProcessingService;
import com.examples.activemq.pubsub.services.ItemProcessingServiceImpl;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.support.converter.MappingJackson2MessageConverter;
import org.springframework.jms.support.converter.MessageConverter;
import org.springframework.jms.support.converter.MessageType;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

@EnableAsync
@EnableJms
@Configuration
public class ItemSubscriberConfiguration {

    @Bean
    public MessageConverter jacksonJmsMessageConverter() {
        MappingJackson2MessageConverter converter = new MappingJackson2MessageConverter();
        converter.setTargetType(MessageType.TEXT);
        converter.setTypeIdPropertyName("_type");
        return converter;
    }

    @Bean
    public ItemProcessingService itemProcessingService(
            @NotNull @Value("${processing-delay}") Long processingDelay) {
        return new ItemProcessingServiceImpl(processingDelay);
    }

    @Bean
    public ItemJmsSubscriber itemJmsSubcriber(@NotNull @Value("${destination}") String destination,
                                              @NotNull @Value("${destination.type}") String destinationType,
                                              ItemProcessingService itemProcessorService) {
        return new ItemJmsSubscriber(destination, destinationType, itemProcessorService);
    }

    @Bean
    public Executor syncExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(2);
        executor.setMaxPoolSize(2);
        executor.setQueueCapacity(500);
        executor.setThreadNamePrefix("async-processing-thread-");
        executor.initialize();
        return executor;
    }

}