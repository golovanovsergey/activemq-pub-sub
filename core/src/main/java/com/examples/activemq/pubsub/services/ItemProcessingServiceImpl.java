package com.examples.activemq.pubsub.services;

import javax.validation.constraints.NotNull;
import java.util.concurrent.TimeUnit;

import com.examples.activemq.pubsub.model.Item;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;

@Slf4j
public class ItemProcessingServiceImpl implements ItemProcessingService {

    private final long processingDelay;

    public ItemProcessingServiceImpl(@NotNull @Value("${processing-delay}") long processingDelay) {
        log.info("ItemProcessingService initialisation processingDelay={}", processingDelay);
        this.processingDelay = processingDelay;
    }

    @Async
    public void processing(Item item) {
        try {
            log.info("{} Processing : {}", Thread.currentThread().getName(), item);
            TimeUnit.MILLISECONDS.sleep(processingDelay);
        } catch (InterruptedException e) {
            log.error("{}", e.getMessage(), e);
        }
    }

}
