package com.examples.activemq.pubsub.configs;

import javax.validation.constraints.NotNull;

import com.examples.activemq.pubsub.services.ItemScheduledSender;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.support.converter.MappingJackson2MessageConverter;
import org.springframework.jms.support.converter.MessageConverter;
import org.springframework.jms.support.converter.MessageType;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableJms
@EnableScheduling
@Configuration
public class ItemPublisherConfiguration {

    @Bean
    public MessageConverter jacksonJmsMessageConverter() {
        MappingJackson2MessageConverter converter = new MappingJackson2MessageConverter();
        converter.setTargetType(MessageType.TEXT);
        converter.setTypeIdPropertyName("_type");
        return converter;
    }

    @Bean
    public ItemScheduledSender itemScheduledSender(JmsTemplate jmsTemplate,
                                                   @NotNull @Value("${destination}") String destination,
                                                   @NotNull @Value("${destination.type}") String destinationType,
                                                   @NotNull @Value("${send-delay}") long sendDelay) {
        return new ItemScheduledSender(jmsTemplate, destination, destinationType, sendDelay);
    }

}