package com.examples.activemq.pubsub.services;

import com.examples.activemq.pubsub.model.Item;

public interface ItemProcessingService {

    void processing(Item item);

}
