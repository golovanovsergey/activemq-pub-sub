package com.examples.activemq.pubsub.services;

import javax.validation.constraints.NotNull;

import com.examples.activemq.pubsub.model.Item;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jms.annotation.JmsListener;

@Slf4j
public class ItemJmsSubscriber {

    private long counter = 0;

    private final ItemProcessingService itemProcessorService;

    public ItemJmsSubscriber(@NotNull @Value("${destination}") String destination,
                             @NotNull @Value("${destination.type}") String destinationType,
                             ItemProcessingService itemProcessorService) {
        this.itemProcessorService = itemProcessorService;
        log.info("ItemJmsSubcriberinitialisation {}:{}", destinationType, destination);
    }

    @JmsListener(destination = "${destination}", containerFactory = "listenerContainerFactory")
    public void receive(Item item) {
        counter++;
        log.info("< {} Received {} : {}", Thread.currentThread().getName(), counter, item);
        itemProcessorService.processing(item);
    }

}
