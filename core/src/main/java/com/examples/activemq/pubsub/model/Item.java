package com.examples.activemq.pubsub.model;

import java.io.Serializable;

public class Item implements Serializable {

    private Long id;
    private String owner;
    private String description;

    public Item() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Item{" +
                "id=" + id +
                ", owner='" + owner + '\'' +
                ", description='" + description + '\'' +
                '}';
    }


    public static Builder newBuilder() {
        return new Item().new Builder();
    }

    public class Builder {

        public Item build() {
            return Item.this;
        }

        public Builder setId(Long id) {
            Item.this.id = id;
            return this;
        }


        public Builder setOwner(String owner) {
            Item.this.owner = owner;
            return this;
        }

        public Builder setDescription(String description) {
            Item.this.description = description;
            return this;
        }

    }
}
