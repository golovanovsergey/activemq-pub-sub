package com.examples.activemq.pubsub.services;

import javax.validation.constraints.NotNull;
import java.util.Date;

import com.examples.activemq.pubsub.model.Item;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.scheduling.annotation.Scheduled;

@Slf4j
public class ItemScheduledSender {

    private final JmsTemplate jmsTemplate;
    private final String destination;
    private final String destinationType;
    private final long id;

    private long counter;

    public ItemScheduledSender(JmsTemplate jmsTemplate,
                               @NotNull @Value("${destination}") String destination,
                               @NotNull @Value("${destination.type}") String destinationType,
                               @NotNull @Value("${send-delay}") long sendDelay) {
        this.jmsTemplate = jmsTemplate;
        this.destination = destination;
        this.destinationType = destinationType;
        id = System.currentTimeMillis();
        counter = 0;
        log.info("ScheduledSender initialisation id={}, destination={}:{}, sendDelay={}",
                id, destinationType, destination, sendDelay);
    }

    @Scheduled(fixedDelayString = "${send-delay}")
    public void send() {
        counter++;
        final Item item = Item.newBuilder()
                .setId(counter)
                .setOwner(Thread.currentThread().getName() + id)
                .setDescription("message from id=" + id + ", date=" + new Date()).build();
        log.info("> sending to {}:{} : {}", destinationType, destination, item);
        jmsTemplate.convertAndSend(destination, item);
    }

}