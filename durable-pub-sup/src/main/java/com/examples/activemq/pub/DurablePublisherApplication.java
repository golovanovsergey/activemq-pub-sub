package com.examples.activemq.pub;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DurablePublisherApplication {

    public static void main(String[] args) {
        SpringApplication.run(DurablePublisherApplication.class, args);
    }

}

